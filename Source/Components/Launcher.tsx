import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import withStyles, { CSSProperties, StyleRulesCallback, WithStyles } from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Warning from "@material-ui/icons/Warning";
import * as React from "react";
import globalTheme from "../Theme";

// tslint:disable:max-line-length no-single-line-block-comment

// tslint:disable-next-line:typedef
const initialState = {
    activeCarouselCard: 0,
};

type LauncherState = Readonly<typeof initialState>;

/**
 * Sample
 */
class Launcher extends React.Component<WithStyles<string>, LauncherState> {
    public readonly state: LauncherState = initialState;

    public constructor(props?: WithStyles<string>) {
        // tslint:disable-next-line:no-any
        super(props as any);

        this.onClickScrollIndicator = this.onClickScrollIndicator.bind(this);
    }

    // tslint:disable-next-line:max-func-body-length
    public render(): JSX.Element {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <main className={classes.content} style={{ position: "relative" }}>
                    <div style={{ position: "relative" }} className="cardCarousel">
                        <div
                            style={{
                                position: "absolute",
                                width: "100%",
                                height: "100%",
                                top: 0,
                                left: 0,
                                zIndex: globalTheme.zIndex.drawer,
                                pointerEvents: "none",
                                display: "flex",
                                alignItems: "center",
                            }}
                            className="cardCarouselOverlay"
                        >
                            {this.state.activeCarouselCard !== 0 && (
                                <Button
                                    variant="fab"
                                    color="primary"
                                    style={{ pointerEvents: "all", marginLeft: 16 }}
                                    className="scrollIndicator"
                                    onClick={this.onClickScrollIndicator}
                                    id="leftScrollIndicator"
                                >
                                    <i className="material-icons">chevron_left</i>
                                </Button>
                            )}
                            <div style={{ flex: 1 }} />
                            {this.state.activeCarouselCard !== 8 /* max - 2 */ && (
                                <Button
                                    variant="fab"
                                    color="primary"
                                    style={{ pointerEvents: "all", marginRight: 16 }}
                                    className="scrollIndicator"
                                    onClick={this.onClickScrollIndicator}
                                    id="rightScrollIndicator"
                                >
                                    <i className="material-icons">chevron_right</i>
                                </Button>
                            )}
                        </div>
                        <GridList className={classes.gridList} spacing={0} id="gridList" style={{ height: 300 }}>
                            <GridListTile
                                className={classes.gridItem}
                                style={{ width: 2 * 8 + highlightCardWidth, height: 3 * 8 + 275, marginLeft: 16 }}
                            >
                                <Card
                                    elevation={8}
                                    style={{
                                        margin: 8,
                                        width: highlightCardWidth,
                                        height: 275,
                                        background: globalTheme.palette.background.paper,
                                    }}
                                >
                                    <CardContent
                                        style={{
                                            background: globalTheme.palette.primary.main,
                                            paddingLeft: 16,
                                            paddingRight: 16,
                                            paddingTop: 16,
                                            paddingBottom: 16,
                                        }}
                                        id="headerContent"
                                    >
                                        <div style={{ display: "flex", flex: 1, alignItems: "center" }}>
                                            <div style={{ flex: 1 }}>
                                                <Typography style={{ fontSize: globalTheme.typography.fontSize + 4 }}>
                                                    Patch Notes
                                                </Typography>
                                            </div>
                                        </div>
                                    </CardContent>
                                    <CardContent style={{ margin: 8, padding: 0 }} id="bodyContent">
                                        <div
                                            style={{
                                                maxHeight: 275 - 26 - 2 * 16 - 2 * 8,
                                                overflow: "auto",
                                                fontSize: globalTheme.typography.fontSize,
                                                lineHeight: "1.65em",
                                            }}
                                            className="scrollableGridCard"
                                        >
                                            <div
                                                style={{ marginLeft: 8, marginRight: 8, marginTop: 0, marginBottom: 0 }}
                                            >
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                                                laoreet maximus iaculis. Maecenas ex justo, aliquam ut vestibulum vitae,
                                                malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Curabitur laoreet maximus iaculis. Maecenas ex justo, aliquam ut
                                                vestibulum vitae, malesuada. Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Curabitur laoreet maximus iaculis. Maecenas ex justo,
                                                aliquam ut vestibulum vitae, malesuada. Lorem ipsum dolor sit amet,
                                                consectetur adipiscing elit. Curabitur laoreet maximus iaculis. Maecenas
                                                ex justo, aliquam ut vestibulum vitae, malesuada. Lorem ipsum dolor sit
                                                amet, consectetur adipiscing elit. Curabitur laoreet maximus iaculis.
                                                Maecenas ex justo, aliquam ut vestibulum vitae, malesuada. Lorem ipsum
                                                dolor sit amet, consectetur adipiscing elit. Curabitur laoreet maximus
                                                iaculis. Maecenas ex justo, aliquam ut vestibulum vitae, malesuada.
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                                                laoreet maximus iaculis. Maecenas ex justo, aliquam ut vestibulum vitae,
                                                malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Curabitur laoreet maximus iaculis. Maecenas ex justo, aliquam ut
                                                vestibulum vitae, malesuada.
                                            </div>
                                        </div>
                                    </CardContent>
                                </Card>
                            </GridListTile>
                            <GridListTile
                                className={classes.gridItem}
                                style={{ width: 2 * 8 + genericCardWidth, height: 3 * 8 + 275 }}
                            >
                                <Card
                                    elevation={8}
                                    style={{
                                        margin: 8,
                                        width: genericCardWidth,
                                        height: 275,
                                        background: "rgba(204, 51, 0, 0.4)",
                                    }}
                                >
                                    <CardContent
                                        style={{
                                            background: "rgba(255, 102, 0, 0.3)",
                                            paddingLeft: 16,
                                            paddingRight: 16,
                                            paddingTop: 16,
                                            paddingBottom: 16,
                                        }}
                                    >
                                        <div style={{ display: "flex", flex: 1, alignItems: "center" }}>
                                            <Warning />
                                            <div style={{ flex: 1, paddingLeft: 16 }}>
                                                <Typography style={{ fontSize: globalTheme.typography.fontSize + 4 }}>
                                                    Breaking News
                                                </Typography>
                                            </div>
                                        </div>
                                    </CardContent>
                                    <CardContent style={{ margin: 8, padding: 0 }}>
                                        <div
                                            style={{
                                                maxHeight: 275 - 26 - 2 * 16 - 2 * 8,
                                                overflow: "auto",
                                                fontSize: globalTheme.typography.fontSize,
                                                lineHeight: "1.65em",
                                            }}
                                            className="scrollableGridCard"
                                            id="testScroller"
                                        >
                                            <div
                                                style={{ marginLeft: 8, marginRight: 8, marginTop: 0, marginBottom: 0 }}
                                            >
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
                                                laoreet maximus iaculis. Maecenas ex justo, aliquam ut vestibulum vitae,
                                                malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Curabitur laoreet maximus iaculis. Maecenas ex justo, aliquam ut
                                                vestibulum vitae, malesuada. Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Curabitur laoreet maximus iaculis. Maecenas ex justo,
                                                aliquam ut vestibulum vitae, malesuada. Lorem ipsum dolor sit amet,
                                                consectetur adipiscing elit. Curabitur laoreet maximus iaculis. Maecenas
                                                ex justo, aliquam ut vestibulum vitae, malesuada.
                                            </div>
                                        </div>
                                    </CardContent>
                                </Card>
                            </GridListTile>
                            <GridListTile
                                className={classes.gridItem}
                                style={{ width: (2 * 8 + 235) * 10, height: 3 * 8 + 275 }}
                            />
                        </GridList>
                    </div>
                </main>
            </div>
        );
    }

    private onClickScrollIndicator(event: React.MouseEvent<HTMLElement>): void {
        if (!isScrolling && this.state.activeCarouselCard >= 0 && this.state.activeCarouselCard <= 8 /* max - 2 */) {
            const gridList: HTMLElement | null = document.getElementById("gridList");
            if (gridList !== null) {
                if (event.currentTarget.id === "leftScrollIndicator") {
                    if (this.state.activeCarouselCard !== 0) {
                        const newCarouselCard: number = this.state.activeCarouselCard - 1;
                        this.setState({ activeCarouselCard: newCarouselCard });
                        const destination: number = getOffsetFromCardNumber(newCarouselCard);
                        scrollTo(gridList, destination, 300);
                    }
                } else if (event.currentTarget.id === "rightScrollIndicator") {
                    if (this.state.activeCarouselCard !== 8 /* max - 2*/) {
                        const newCarouselCard: number = this.state.activeCarouselCard + 1;
                        this.setState({ activeCarouselCard: newCarouselCard });
                        const destination: number = getOffsetFromCardNumber(newCarouselCard);
                        scrollTo(gridList, destination, 300);
                    }
                }
            }
        }
    }
}

const styles: StyleRulesCallback = (theme: Theme): Record<string, CSSProperties> => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: "hidden",
        position: "relative",
        display: "flex",
        height: "100vh",
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: 0,
        minWidth: 0,
    },
    gridList: {
        flexWrap: "nowrap",
        transform: "translateZ(0)",
        overflowX: "hidden",
        overflowY: "hidden",
    },
    gridPaper: {
        backgroundColor: theme.palette.background.paper,
    },
    toolbar: theme.mixins.toolbar,
});

export default withStyles(styles)(Launcher);

// tslint:disable:one-variable-per-declaration prefer-const typedef
const highlightCardWidth: number = 486;
const genericCardWidth: number = 235;
const cardMargin: number = globalTheme.spacing.unit;

function getOffsetFromCardNumber(card: number): number {
    switch (card) {
        case 0:
            return 0;
        case 1:
            return genericCardWidth + 2 * cardMargin;
        default:
            return (card - 2) * (genericCardWidth + 2 * cardMargin) + (highlightCardWidth + 2 * cardMargin);
    }
}

let isScrolling = false;
function scrollTo(element: Element, to: number, duration: number): void {
    isScrolling = true;
    let start = element.scrollLeft;
    let change = to - start;
    let currentTime = 0;
    let increment = 20;

    const animateScroll = (): void => {
        currentTime += increment;
        const val = easeInOutQuad(currentTime, start, change, duration);
        element.scrollLeft = val;
        if (currentTime < duration) {
            setTimeout(animateScroll, increment);
        } else {
            isScrolling = false;
            element.scrollLeft = to;
        }
    };
    animateScroll();
}

function easeInOutQuad(currentTime: number, startValue: number, delta: number, duration: number): number {
    let t: number = currentTime;
    t /= duration / 2;
    if (t < 1) {
        return (delta / 2) * t * t + startValue;
    }
    t -= 1;

    return (-delta / 2) * (t * (t - 2) - 1) + startValue;
}
