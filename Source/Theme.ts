import createMuiTheme, { Theme } from "@material-ui/core/styles/createMuiTheme";

/**
 * Global launcher theme
 */
export const globalTheme: Theme = createMuiTheme({
    palette: {
        type: "dark",
        primary: {
            light: "#4f5b62",
            main: "#263238",
            dark: "#000a12",
            contrastText: "#fff",
        },
        secondary: {
            light: "#63a4ff",
            main: "#1976d2",
            dark: "#004ba0",
            contrastText: "#fff",
        },
        background: {
            default: "#131c23",
            paper: "#1d272e",
        },
    },
});
export default globalTheme;
