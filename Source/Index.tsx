/**
 * Electron Renderer Process / React Entry Point
 */
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import * as React from "react";
import * as ReactDOM from "react-dom";
import Launcher from "./Components/Launcher";
import globalTheme from "./Theme";

// tslint:disable:no-import-side-effect
import "./Styles/Style.scss";
// tslint:enable:no-import-side-effect

ReactDOM.render(
    <MuiThemeProvider theme={globalTheme}>
        <Launcher />
    </MuiThemeProvider>,
    document.getElementById("root")
);
