/**
 * Base webpack config used across other specific configs
 */
const path = require("path");

module.exports = {
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
        ],
    },

    output: {
        path: path.join(__dirname, "..", "Build"),
        filename: "Bundle.js",
    },

    mode: "development",

    resolve: {
        extensions: [".js", ".ts", ".tsx", ".json"],
        modules: [path.join(__dirname, "..", "Source"), path.join(__dirname, "..", "node_modules")],
    },

    plugins: [],

    externals: {},

    optimization: {
        nodeEnv: false,
    },
};
