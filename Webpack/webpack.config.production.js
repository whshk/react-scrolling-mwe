/**
 * Build config for electron render process entry point
 */
const extractTextWebpackPlugin = require("extract-text-webpack-plugin");
const htmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpackMerge = require("webpack-merge");

const baseConfig = require("./webpack.config.base");

module.exports = webpackMerge(baseConfig, {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: extractTextWebpackPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader!sass-loader",
                }),
            },
            {
                test: /\.css$/,
                use: extractTextWebpackPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader",
                }),
            }
        ],
    },

    devtool: "cheap-module-source-map",

    entry: path.join(__dirname, "..", "Source", "Index.tsx"),

    plugins: [
        new htmlWebpackPlugin({
            template: path.join(__dirname, "..", "Source", "Index.html"),
            filename: "Index.html",
        }),
        new extractTextWebpackPlugin("Style.css"),
    ],

    mode: "development",

    target: "web",
});
